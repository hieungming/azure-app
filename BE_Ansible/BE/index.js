import express, { json } from 'express';
import cors from 'cors';

const app = express();

const PORT = 80;
app.use(cors())


// app.use(bodyParser.json());
const abc = [{
    id: "1",
    name: "Nguyen Tuan Vu"
}];
app.get('/', (req, res) => res.json(abc));

app.listen(PORT, () => console.log(`Server running on port: http://localhost:${PORT}`));

