variable "location" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "sku_tier" {
  type = string
}

variable "sku_size" {
  type = string
}

variable "app_service_name" {
  type = string
}

variable "app_service_plan_name" {
  type = string
}

# variable "as_name" {
#   type = string
# }

variable "vm_name" {
  type = string
}

variable "size" {
  type = string
}

variable "admin_username" {
  type = string
}

variable "caching" {
  type = string
}

variable "storage_account_type" {
  type = string
}

variable "publisher" {
  type = string
}

variable "offer" {
  type = string
}

variable "sku" {
  type = string
}

variable "backend_storage_account_name" {
  type = string
}

variable "backend_resource_group_name" {
  type = string
}

variable "backend_container_name" {
  type = string
}

variable "backend_state_file" {
  type = string
}

variable "si_version" {
  type = string
}

variable "username" {
  type = string
}

variable "public_key" {
  type = string
}

variable "loadbalancer_name" {
  type = string
}

variable "fe_ip_config_name" {
  type = string
}

variable "backend_address_pool_name" {
  type = string
}

variable "ip_config_name" {
  type = string
}

variable "lb_probe_name" {
  type = string
}

variable "port_number" {
  type = number
}

variable "protocol" {
  type = string
}

variable "lb_rule_name" {
  type = string
}

variable "fe_port" {
  type = number
}

variable "be_port" {
  type = number
}

variable "sku_lb" {
  type = string
}

variable "asp_state_file" {
  type = string
}