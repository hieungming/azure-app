output "network_interface_id" {
  value = module.network_interface.network_interface_id
}

output "public_ip_address_for_vm_id" {
  value = module.public_ip.public_ip_for_vm_id
}

output "public_ip_address_for_lb_id" {
  value = module.public_ip.public_ip_for_lb_id
}