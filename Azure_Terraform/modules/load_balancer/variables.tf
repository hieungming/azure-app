variable "location" {
 type = string 
}

variable "resource_group_name" {
 type = string 
}

variable "ip_config_name" {
 type = string 
}

variable "lb_name" {
 type = string 
}

variable "fname" {
 type = string 
}

variable "bap_name" {
 type = string 
}

variable "public_ip_address_id" {
  type = string 
}

variable "network_interface_id" {
  type = string 
}

variable "sku" {
  type = string 
}

variable "lb_probe_name" {
  type = string 
}

variable "port_number" {
  type = number 
}

variable "protocol" {
  type = string 
}

variable "lb_rule_name" {
  type = string 
}

variable "fe_port" {
  type = number 
}

variable "be_port" {
  type = number 
}
