variable "public_ip_for_vm_name" {
  type = string
}

variable "public_ip_for_lb_name" {
  type = string
}

variable "location" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "allocation_method" {
  type = string
}

variable "sku" {
  type = string
}


