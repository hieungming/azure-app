output "public_ip_for_vm_id" {
  value = azurerm_public_ip.public_ip_for_VM.id
}

output "public_ip_for_lb_id" {
  value = azurerm_public_ip.public_ip_for_LB.id
}