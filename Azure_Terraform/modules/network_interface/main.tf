resource "azurerm_network_interface" "network_interface" {
  name                = var.ni_name
  location            = var.location
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = var.ip_config_name
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = var.private_ip_address_allocation
    public_ip_address_id = var.public_ip_address_id
  }

  depends_on = [
    azurerm_subnet_network_security_group_association.ns_sga,
  ]
}

resource "azurerm_subnet_network_security_group_association" "ns_sga" {
  subnet_id                 = var.subnet_id
  network_security_group_id = var.network_security_group_id
}

resource "azurerm_network_interface_security_group_association" "ni_sga" {
  network_interface_id      = azurerm_network_interface.network_interface.id
  network_security_group_id = var.network_security_group_id
}