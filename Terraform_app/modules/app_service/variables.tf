variable "location" {
  type = string
  description = ""
}

variable "resource_group_name" {
  type = string
  description = ""
}

variable "app_service_name" {
  type = string
  description = ""
}

variable "app_service_plan_name" {
  type = string
  description = ""
}

variable "sku_tier" {
  type = string
}

variable "sku_size" {
  type = string
}
