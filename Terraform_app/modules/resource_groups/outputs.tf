output "rg_name" {
    value = azurerm_resource_group.az_lab_rg.name
}

output "location" {
    value = azurerm_resource_group.az_lab_rg.location
}