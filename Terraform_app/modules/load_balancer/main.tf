resource "azurerm_lb" "lb" {
  name = var.lb_name
  location = var.location
  resource_group_name = var.resource_group_name
  sku = var.sku
  frontend_ip_configuration {
    name = var.fname
    public_ip_address_id = var.public_ip_address_id
  }
}

resource "azurerm_lb_backend_address_pool" "bap" {
  loadbalancer_id = azurerm_lb.lb.id
  name            = var.bap_name
}

resource "azurerm_network_interface_backend_address_pool_association" "bapa" {
  network_interface_id    = var.network_interface_id
  ip_configuration_name   = var.ip_config_name
  backend_address_pool_id = azurerm_lb_backend_address_pool.bap.id
}

resource "azurerm_lb_probe" "lb_probe" {
  loadbalancer_id = azurerm_lb.lb.id
  name            = var.lb_probe_name
  port            = var.port_number
  protocol = var.protocol
}

resource "azurerm_lb_rule" "lb_rule" {
  loadbalancer_id                = azurerm_lb.lb.id
  name                           = var.lb_rule_name
  protocol                       = var.protocol
  frontend_port                  = var.fe_port
  backend_port                   = var.be_port
  frontend_ip_configuration_name = azurerm_lb.lb.frontend_ip_configuration[0].name
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.bap.id]
  probe_id                       = azurerm_lb_probe.lb_probe.id
}