variable "resource_group_name" {
  type = string
  default = "ojt-lab-rg"
}

variable "location" {
  type = string
  default = "East US"
}

variable "storage_account_name" {
  type = string
  default = "ojtlabsa"
}

variable "account_tier" {
  type = string
  default = "Standard"
}

variable "account_replication_type" {
  type = string
  default = "LRS"
}

variable "storage_container_name" {
  type = string
  default = "ojt-lab-container"
}

variable "container_access_type" {
  type = string
  default = "private"
}
