terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.49.0"
    }
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

module "resource_group" {
  source = "../../modules/resource_groups"
  resource_group_name = var.resource_group_name
  location = var.location
}

module "storage_account" {
  source = "../../modules/storage_accounts"
  storage_account_name = var.storage_account_name
  rg_name = module.resource_group.rg_name
  location = module.resource_group.location
  account_tier = var.account_tier
  account_replication_type = var.account_replication_type
  storage_container_name = var.storage_container_name
  container_access_type = var.container_access_type
}
