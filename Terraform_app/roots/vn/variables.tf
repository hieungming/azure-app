variable "vn_name" {
  type = string
}

variable "address_space" {
  type = list(string)
}

variable "subnet_name" {
  type = string
}

variable "address_prefixes" {
  type = list(string)
}

variable "security_group_name" {
  type = string
}

variable "network_interface_name" {
  type = string
}

variable "ip_config_name" {
  type = string
}

variable "private_ip_address_allocation" {
  type = string
}

variable "public_ip_for_vm_name" {
  type = string
}

variable "public_ip_for_lb_name" {
  type = string
}

variable "allocation_method" {
  type = string
}

variable "location" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "sku" {
  type = string
}
